
<div id="carousel12" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carousel12" data-slide-to="0" class="active"></li>
    <li data-target="#carousel12" data-slide-to="1"></li>
    <li data-target="#carousel12" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner" role="listbox">
    <div class="carousel-item active">
      <div class="bg-primary pd-30 ht-300 d-flex align-items-center">
        <div class="tx-white">
          <p class="tx-uppercase tx-11 tx-spacing-2">Recent Article</p>
          <h5 class="lh-5 mg-b-20">20 Best Travel Tips After 5 Years Of Traveling The World</h5>
          <nav class="nav flex-row tx-13">
            <a href="" class="tx-white-8 hover-white pd-l-0 pd-r-5">Edit</a>
            <a href="" class="tx-white-8 hover-white pd-x-5">Unpublish</a>
            <a href="" class="tx-white-8 hover-white pd-x-5">Delete</a>
          </nav>
        </div>
      </div><!-- d-flex -->
    </div><!-- carousel-item -->
    <div class="carousel-item">...</div>
    <div class="carousel-item">...</div>
  </div><!-- carousel-inner -->
</div><!-- carousel -->