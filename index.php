<!DOCTYPE html>
<html>

<head>

	<title>Website Build traveler</title>

	<!-- menghubungkan dengan file css -->
	<link rel="stylesheet" type="text/css" href="index.css">

	<!-- menghubungakn dengan templete -->
	<link rel="stylesheet" href="template/app/css/bracket.css">

	<!-- menggabungkan dengan bootstrap -->
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

	<!-- menghubungkan dengan file jquery -->
	<script type="text/javascript" src="jquery-3.4.1.min.js"></script>

</head>

<body>
	<nav class="navbar fixed-top navbar-expand-lg navbar-light">
		<a class="navbar-brand" style="color: white" href="#">Flo</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
			aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="nav navbar-nav ml-auto">
				<li class="nav-item active">
					<a class="text-uppercase" href="index.php?page=home">Home <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item">
					<a class="text-uppercase" href="index.php?page=traveling">Traveling</a>
				</li>
				<li class="nav-item">
					<a class="text-uppercase" href="index.php?page=home#about">About</a>
				</li>
				<li class="nav-item">
					<a class="text-uppercase" href="index.php?page=home#contact">Contact</a>
				</li>
			</ul>
		</div>
	</nav>

	<!-- <div class="navbar">
		<ul>
			<li><a href="index.php?page=home">HOME</a></li>
			<li><a href="index.php?page=traveling">Traveling</a></li>
			<li><a href="inndex.php?page=home#about">About</a></li>
			<li><a href="index.php?page=contact#contact">Contact</a></li>
		</ul>
	</div>
	 -->
	<div class="content">
		<?php 
		if(isset($_GET['page'])){
			$page = $_GET['page'];
	
			switch ($page) {
				case 'home':
					include "halaman/home.php";
					break;
				case 'traveling':
					include "halaman/traveling.php";
					break;			
				default:
					echo "<center><h3>Maaf. Halaman tidak di temukan !</h3></center>";
					break;
			}
		}else{
			include "halaman/home.php";
		}
		?>
	</div>
</body>

</html>